// Simple version to fetch a page and specify tls version
// usage : tlsget -url https://localhost:8080 -version 1.0
// where version is the tls version 1.0 - 1.3
package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	versionMap := map[string]uint16{
		"1.0": tls.VersionTLS10,
		"1.1": tls.VersionTLS11,
		"1.2": tls.VersionTLS12,
		"1.3": tls.VersionTLS13,
	}

	vflag := flag.String("version", "1.0", "choose a TLS version 1.0 - 1.3")
	url := flag.String("url", "", "URL to fetch")
	flag.Parse()

	if *url == "" {
		fmt.Println("No -url specified")
		return
	}
	tlsVer, ok := versionMap[*vflag]
	if !ok {
		fmt.Printf("%s is an invalid version. Defaulting to max supported.\n", *vflag)
	}
	fmt.Printf("%s %d\n", *vflag, tlsVer)

	config := &tls.Config{MaxVersion: tlsVer, InsecureSkipVerify: true}
	tr := http.Transport{TLSClientConfig: config}
	client := http.Client{Transport: &tr}
	resp, err := client.Get(*url)

	if err != nil {
		fmt.Printf("Error: %s", err)
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	fmt.Printf("%s", string(body))

}
